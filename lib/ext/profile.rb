require_dependency 'profile'

Profile.class_eval do
  has_many :questions, -> { order(:start_date) }, :source => 'articles', :class_name => 'PairwisePlugin::PairwiseContent'
end
