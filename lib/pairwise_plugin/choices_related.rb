class PairwisePlugin::ChoicesRelated < ActiveRecord::Base

  self.table_name = :pairwise_plugin_choices_related

  belongs_to :question, :class_name => 'PairwisePlugin::PairwiseContent'
  belongs_to :user

  validates_presence_of :question, :choice_id, :parent_choice_id

  attr_accessible :question, :choice_id, :parent_choice_id

  def self.related_choices_for choice_id
     PairwisePlugin::ChoicesRelated.where(choice_id: choice_id) + PairwisePlugin::ChoicesRelated.where(parent_choice_id: choice_id)
  end

end
