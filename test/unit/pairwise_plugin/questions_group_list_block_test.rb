require 'test_helper'
require "#{Rails.root}/plugins/pairwise/test/fixtures/pairwise_content_fixtures"

class PairwisePlugin::QuestionsGroupListBlockTest < ActiveSupport::TestCase

  def setup
    @profile = create_user('testing').person

    PairwisePlugin::PairwiseContent.any_instance.stubs(:send_question_to_service).returns(true)

    @question1 = PairwisePlugin::PairwiseContent.new(:name => 'Question 1', :profile => @profile, :pairwise_question_id => 1, :body => 'Body 1')
    @question1.stubs(:valid?).returns(true)
    @question1.save!

    @question2 = PairwisePlugin::PairwiseContent.new(:name => 'Question 2', :profile => @profile, :pairwise_question_id => 2, :body => 'Body 2')
    @question2.stubs(:valid?).returns(true)
    @question2.save!

    @block = PairwisePlugin::QuestionsGroupListBlock.create(:title => "Pairwise Question Block")
    @profile.boxes.first.blocks << @block
    @block.save!
  end

  should 'list available questions' do
    assert_equivalent [@question1, @question2], @block.available_questions
  end

  should 'start with selected questions in available questions list' do
    @block.questions_ids = @question1.id
    assert_equal [@question1, @question2], @block.available_questions
  end

end
